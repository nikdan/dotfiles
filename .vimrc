
set tabstop=2
set shiftwidth=2
set smarttab
set et 
set ai
set pastetoggle=

set wrap
set cin

" search & math
set showmatch
set hlsearch
set incsearch
set ignorecase

set columns=100                  " amount of colomns

set lz                          " lazy printing in script execution


set list listchars=tab:->,trail:· " print tab characters in the begin of lines


if !has('gui_running')          " disable mouse in nw mode
set mouse=
endif

syntax on
colorscheme jellybeans
