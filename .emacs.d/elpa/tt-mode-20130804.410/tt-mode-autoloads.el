;;; tt-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (tt-mode) "tt-mode" "tt-mode.el" (21758 2181 444100
;;;;;;  381000))
;;; Generated autoloads from tt-mode.el

(autoload 'tt-mode "tt-mode" "\
Major mode for editing Template Toolkit files

\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.tt\\'" . tt-mode))

;;;***

;;;### (autoloads nil nil ("tt-mode-pkg.el") (21758 2181 517420 266000))

;;;***

(provide 'tt-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; tt-mode-autoloads.el ends here
